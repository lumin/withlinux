class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        vtoi = dict()
        for i, v in enumerate(nums):
            idx = vtoi.get(target - v, None)
            if None!=idx: return [i, idx]
            else: vtoi[v] = i
        return False;  # O(n/2)

s = Solution()
print(s.twoSum([3,3], 6))
print(s.twoSum([2,7,11,15], 13))
