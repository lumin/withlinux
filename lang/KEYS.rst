LANGUAGE
========

* Important Languages to me::

  Python3         *****
  C/C++            ****
  Julia            ****

* I want to learn more about::

  Julia           ***** (LLVM IR)
  Rust             ****
  Python3           ***

TOOL LANGUAGE
=============

* Most important tool languages for me::

  ReST            *****
  Tex             *****
  Markdown          ***
  Awk               ***
  Make               **
