## With Linux - Quite Messy Collection of Personal Snippets

> KISS: Keep It Simple, Stupid

This repository was created on 2014 June 28, for collecting personal snippets
about code, hints, and hacks about Linux.

To search among these memos, lookup keywords in this repo with the rust utility
`ripgrep`. Or search some keywords with the script `search` as long as you have
created an xapian database with `./maintain xdb`.

## LICENSE

Copyright (C) 2014-2019 M. Zhou, MIT/Expat License.
