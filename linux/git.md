Git tips
===

## Automatic pushing after commit

```
vim .git/hooks/post-commit
> git push --mirror origin
chmod +x .git/hooks/post-commit
```
